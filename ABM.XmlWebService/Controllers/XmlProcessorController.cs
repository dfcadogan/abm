﻿using System.Xml.Linq;
using ABM.XmlProcessing;
using Microsoft.AspNetCore.Mvc;

namespace ABM.XmlWebService.Controllers
{
    // Tested using Content-Type=application/xml in Postman
    [ApiController]
    [Route("[controller]")]
    public class XmlProcessorController : ControllerBase
    {
        [HttpPost]
        public int PostXml([FromBody]XElement xml)
        {
            var processor = new InputDocumentProcessor();

            return processor.ValidateInputDocument(xml);
        }
    }
}
