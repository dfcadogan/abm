﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace ABM.EdifactTextProcessing.Tests
{
    class EdifactTextProcessorTests
    {

        private readonly string _edifactText = @"UNA:+.? '
UNB+UNOC:3+2021000969+4441963198+180525:1225+3VAL2MJV6EH9IX+KMSV7HMD+CUSDECU-IE++1++1'
UNH+EDIFACT+CUSDEC:D:96B:UN:145050'
BGM+ZEM:::EX+09SEE7JPUV5HC06IC6+Z'
LOC+17+IT044100'
LOC+18+SOL'
LOC+35+SE'
LOC+36+TZ'
LOC+116+SE003033'
DTM+9:20090527:102'
DTM+268:20090626:102'
DTM+182:20090527:102'
";

        /// <summary>
        //      Test 1. Taking an EDIFACT message text, write some code to parse out the all the LOC 
        //              segments and populate an array with the 2nd and 3rd element of each segment
        /// </summary>
        [Test]
        public void GetLocCodes_ReturnsLocItems()
        {
            EdifactTextProcessor processor = new EdifactTextProcessor();

            List<string> matches = processor.GetLocCodes(_edifactText);

            Assert.AreEqual(10, matches.Count);
        }

    }
}
