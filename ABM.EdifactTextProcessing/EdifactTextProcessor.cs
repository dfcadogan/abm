﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace ABM.EdifactTextProcessing
{
    public class EdifactTextProcessor
    {
        public List<string> GetLocCodes(string edifactText)
        {
            List<string> matches = new List<string>();

            // Use regular expressions to parse the content
            var locMatches = Regex.Matches(edifactText, @"LOC\+(?<locItems>[^']+')");

            foreach (Match match in locMatches)
            {
                // Assuming that there are always *two* items after the "LOC" - otherwise would index into the first
                // two items only when adding to the matches list
                matches.AddRange(match.Groups["locItems"].Value.Split("+"));
            }
            return matches;
        }
    }
}
