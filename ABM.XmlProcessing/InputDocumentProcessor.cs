﻿using System.Xml.Linq;
using System.Xml.XPath;

namespace ABM.XmlProcessing
{
    public class InputDocumentProcessor
    {
        private const string _xpathCommandQuery = "//Declaration[@Command='DEFAULT']";
        private const string _xpathSiteQuery = "//Declaration/DeclarationHeader/SiteID";

        public int ValidateInputDocument(XElement element)
        {
            if (element.XPathSelectElement(_xpathCommandQuery)?.Value == null)
                return -1;

            if (element.XPathSelectElement(_xpathSiteQuery)?.Value != "DUB")
                return -2;

            return 0;
        }
    }
}
