﻿using NUnit.Framework;
using System.Xml.Linq;

namespace ABM.XmlProcessing.Tests
{
	public class InputDocumentProcessorTests
    {
		private string _xmlString = @"<InputDocument>
	<DeclarationList>
		<Declaration Command=""{0}"" Version=""5.13"">
			<DeclarationHeader>
				<Jurisdiction>IE</Jurisdiction>
				<CWProcedure>IMPORT</CWProcedure>
							<DeclarationDestination>CUSTOMSWAREIE</DeclarationDestination>
				<DocumentRef>71Q0019681</DocumentRef>
				<SiteID>{1}</SiteID>
				<AccountCode>G0779837</AccountCode>
			</DeclarationHeader>
		</Declaration>
	</DeclarationList>
</InputDocument>
";


		[Test]
		public void ValidateInputDocument_IsValid_ReturnsZero()
		{
			var xmlFragment = XElement.Parse(string.Format(_xmlString, "DEFAULT", "DUB"));

			var processor = new InputDocumentProcessor();

			Assert.AreEqual(0, processor.ValidateInputDocument(xmlFragment));
		}

		[Test]
		public void ValidateInputDocument_InvalidCommand_ReturnsMinusOne()
		{
			var xmlFragment = XElement.Parse(string.Format(_xmlString, "NOTDEFAULT", "DUB"));

			var processor = new InputDocumentProcessor();

			Assert.AreEqual(-1, processor.ValidateInputDocument(xmlFragment));
		}
		[Test]
		public void ValidateInputDocument_InvalidSiteId_ReturnsMinusTwo()
		{
			var xmlFragment = XElement.Parse(string.Format(_xmlString, "DEFAULT", "CORK"));

			var processor = new InputDocumentProcessor();

			Assert.AreEqual(-2, processor.ValidateInputDocument(xmlFragment));
		}
	}

}
