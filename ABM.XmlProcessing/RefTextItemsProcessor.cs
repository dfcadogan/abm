﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Xml.XPath;

namespace ABM.XmlProcessing
{
    public class RefTextItemsProcessor
    {
        // Defining a specific query to include the refcodes requested - could customise this to pass in the individual
        // refcodes to make it more extensible for other refcode types

        private const string _xpathQuery = "//Reference[@RefCode='MWB' or @RefCode='TRV' or @RefCode='CAR']/RefText[text()]";

        public List<string> GetRefTextItems(XElement element)
        {
            List<string> refCodes = new List<string>();

            var results = element.XPathSelectElements(_xpathQuery);

            foreach (var item in results)
            {
                refCodes.Add(item.Value);
            }
            return refCodes;
        }
    }
}

